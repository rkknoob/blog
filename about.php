<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!--Navbar-->
        <?php include_once('includes/navbar.php')?>
            <header  class="page-title jarallax"   data-speed="0.5" style="background-image: url(https://images.unsplash.com/photo-1519397154350-533cea5b8bff?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=009599aa0979619a04f21ec0d5486955&auto=format&fit=crop&w=1450&q=80);">
                <div class="page-image">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center ">
                            <h1 class="display-4 font-weight-bold">เกี่ยวกับเรา</h1>
                            <p class="lead">"คุณไม่เก่งตั้งแต่เริ่ม แต่คุณต้องเริ่มก่อน ถึงจะเก่ง"</p>
                        </div>
                    </div>
                </div>
                </div>
            </header>

            <!-- Section TODO  py คือ padding แกน y-->
    <section class="container py-5">
        <div class="row">
            <div class="col-lg-6 py-3 p-lg-0">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZWjEMjiagcg?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div> 
            <div class="col-lg-6">
                <h2>ทำความรู้จักกับเราให้ดียิ่งขึ้น...</h2>
                <p>เราทำการสอนการเขียนเว็บไซต์สำหรับมือใหม่ ด้วยภาษาต่างๆได้แก่ AngularJS Firebase VueJs2 HTML5 CSS3 PHP MySQLi Laravel5 Bootstrap4</p>
                <br>
                
                <h3>เราคาดหวังไว้ว่า...</h3>
                จะสอนนักเรียนทุกคน ให้สามารถสร้างเว็บไซต์ขึ้นมาด้วยตัวเอง และเรียนรู้องค์ประกอบ ทุกอย่างที่จำเป็นต่อการเริ่มสร้างเว็บไซต์ขึ้นมา เพื่อให้สามารถประกอบอาชีพ, เข้าสมัครงาน, ทำโปรเจคจบ, หรือทำโปรเจคที่ตัวเองคาดหวังไว้ ให้สำเร็จ
            </div>  
        </div>
    </section>

    <section  class="position-relative py-5 jarallax"   data-speed="0.5" style="background-image: url(https://images.unsplash.com/photo-1519241047957-be31d7379a5d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=14bceaef9e3406f4b66fb49b3b9fe736&auto=format&fit=crop&w=1350&q=80);">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="display-4 font-weight-bold text-white">Timeline About Us</h1>
                  
                </div>
            </div>
        </div>
    </section>

    <section class="container py-5">
        <div class="row">
            <div class="col-12">
                <ul class="time-line">
                    <li>
                        <div class="timeline-badge">
                            <p class="text-info">18 Set 1988</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
  
   



            
                       
    <?php include_once('includes/footer.php')?>
        
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
