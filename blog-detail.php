<?php 
require_once('Connect/connectdb.php');

$sql = "SELECT * FROM articles WHERE id= '".$_GET['id']."' AND `status` = '1'";
$result = $conn->query($sql) or die ($conn->error);


if ($result->num_rows > 0){
$row = $result->fetch_assoc();
}else {

   header('Location:blog.php');
}



?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
</head>

<body>
    <!--Navbar-->
    <?php include_once('includes/navbar.php')?>
    <header class="jarallax" data-jarallax='{ "speed": 0.6 }'
        style="background-image: url(<?php echo $row['image'] ?>);">
        <div class="page-image">
        <div class="col-12">
            <h1 class="display-4 font-weight-bold"><?php echo $row['subject'] ?></h1>
            <p class="lead"><?php echo $row['sub_title'] ?></p>
        </div>
        </div>
    </header>



    <section class="container blog-content">
        <div class="row">
            <div class="col-12">
            <?php echo $row['detail'] ?>

            </div>
            <div class="col-12">
                <hr>
                <p class="text-right text-muted"><?php echo date_format(new DateTime($row['updated_at']),"j F Y");  ?></p>
            </div>
            <div class="col-12">
                <div class="owl-carousel owl-theme">
                    <section class="col-12 p-2">
                        <div class="card">
                            <a href="#" class="warpper-card-img">
                                <img src="https://images.unsplash.com/photo-1562476584-14479da1d9ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                                    class="card-img-top">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.Some quick example text to build on the card title and make up the bulk of the card's content.</p>

                            </div>
                            <div class="p-3">
                                <a href="#" class="btn btn-primary btn-block">Go somewhere</a>
                            </div>

                        </div>
                    </section>
                    <section class="col-12 p-2">
                        <div class="card">
                            <a href="#" class="warpper-card-img">
                                <img src="https://images.unsplash.com/photo-1562476584-14479da1d9ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                                    class="card-img-top">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>

                            </div>
                            <div class="p-3">
                                <a href="#" class="btn btn-primary btn-block">Go somewhere</a>
                            </div>

                        </div>
                    </section>
                    <section class="col-12 p-2">
                        <div class="card">
                            <a href="#" class="warpper-card-img">
                                <img src="https://images.unsplash.com/photo-1562476584-14479da1d9ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                                    class="card-img-top">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>

                            </div>
                            <div class="p-3">
                                <a href="#" class="btn btn-primary btn-block">Go somewhere</a>
                            </div>

                        </div>
                    </section>
                    <section class="col-12 p-2">
                        <div class="card">
                            <a href="#" class="warpper-card-img">
                                <img src="https://images.unsplash.com/photo-1562476584-14479da1d9ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                                    class="card-img-top">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>

                            </div>
                            <div class="p-3">
                                <a href="#" class="btn btn-primary btn-block">Go somewhere</a>
                            </div>

                        </div>
                    </section>
                    <section class="col-12 p-2">
                        <div class="card">
                            <a href="#" class="warpper-card-img">
                                <img src="https://images.unsplash.com/photo-1562476584-14479da1d9ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                                    class="card-img-top">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the
                                    bulk of the card's content.</p>

                            </div>
                            <div class="p-3">
                                <a href="#" class="btn btn-primary btn-block">Go somewhere</a>
                            </div>

                        </div>
                    </section>

                </div>
            </div>

        </div>
        <div class="col-12">
                <div class="fb-comments" 
                data-width="100%"
                data-href="https://developers.facebook.com/docs/plugins/comments#configurator" 
                data-numposts="5"></div>
                <div id="fb-root"></div>
        </div>
    </section>



    <!-- Section Blog -->

    <!-- Blog-->



   



    <?php include_once('includes/footer.php')?>


    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.js"></script>
    <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
    <script src="node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
    <script src="assets/js/main.js"></script>

    <script>
    $(document).ready(function(){

        $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        nav:true,
        responsive:{
            0:{
                items:1,
           
            },
            600:{
                items:2,
              
            },
            1000:{
                items:3,
        
            }
        }
    });
    


    });
    
    </script>
   
</body>

</html>