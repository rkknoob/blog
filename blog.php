<?php 
require_once('Connect/connectdb.php');

if(isset($_GET['tag'])){

    $tag = $_GET['tag'];
}else {

    $tag = 'all';
}

$sql = "SELECT * FROM `articles` WHERE `tag` LIKE '%".$tag."%' AND `status` = '1'";
$result = $conn->query($sql);


if (!$result) {
    header('Location: blog.php');
}

?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
        <link rel="stylesheet" href="node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
    </head>
    <body>
        <!--Navbar-->
        <?php include_once('includes/navbar.php')?>
            <header  class="jarallax"   data-speed="0.5" style="background-image: url(https://images.unsplash.com/photo-1562401068-d5affeaca93d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1219&q=80);">
                <div class="page-image">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center ">
                            <h1 class="display-4 font-weight-bold">บทความ</h1>
                            <p class="lead">"คุณไม่เก่งตั้งแต่เริ่ม แต่คุณต้องเริ่มก่อน ถึงจะเก่ง"</p>
                        </div>
                    </div>
                </div>
                </div>
            </header>

            <!-- Blog-->
    <section class="container py-5">
        <div class="row">
            <div class="col-12 text-center ">
                <div class="button-blog det">
                <a href="blog.php?tag=all">
                        <button class="btn btn-primary <?php echo $tag == 'all' ? 'active': '' ?> ">ทั้งหมด</button>
                </a>
                <a href="blog.php?tag=html">
                <button class="btn btn-primary ">HTML</button>
                </a>
                <a href="blog.php?tag=css">
                <button class="btn btn-primary">CSS</button>
                </a>
                <a href="blog.php?tag=javascript">
                <button class="btn btn-primary">Javascript</button>
                </a>
                <a href="blog.php?tag=php">
                <button class="btn btn-primary">PHP</button>
                </a>
                <a href="blog.php?tag=mysql">
                <button class="btn btn-primary">MySql</button>
                </a>
             </div>  
        </div>
    </section>

    <section class="container py-5">
            <div class="row">
                <?php if($result->num_rows){
                    while($row = $result->fetch_assoc()){
                        ?>
                        <section class="col-12 col-sm-6 col-md-4 p-2">
                            <div class="card">
                                    <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="warpper-card-img">
                                    <img src="<?php echo $row['image'] ?>" class="card-img-top">
                                    </a>
                                    <div class="card-body">
                                      <h5 class="card-title"> <?php echo $row['subject'] ?></h5>
                                      <p class="card-text"><?php echo $row['sub_title'] ?></p>
                                      
                                    </div>
                                    <div class="p-3">
                                            <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                                    </div>
                    
                            </div>
                            </section> 
                            <?php } }else { ?>
                             <section class="col-12">
                                  <p class="text-center">ไม่มีข้อมูล</p>
                              </section>  
                            <?php 
                            }
                            ?>     
                        </div>
                    </div>
                </section>





    <?php include_once('includes/footer.php')?>

            
        
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
        <script src="node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
