<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!--Navbar-->
        <?php include_once('includes/navbar.php')?>
            <header  class="jarallax"  data-speed="0.5" style="background-image: url(https://images.unsplash.com/photo-1519397154350-533cea5b8bff?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=009599aa0979619a04f21ec0d5486955&auto=format&fit=crop&w=1450&q=80);">
                <div class="page-image">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center ">
                            <h1 class="display-4 font-weight-bold">ติดต่อเรา</h1>
                            <p class="lead">บริษัท xxxxx. xxxxxx</p>
                        </div>
                    </div>
                </div>
            </div>
            </header>

            <section class="container py-5">
                    <div class="row text-center">
                    <div class="col-12">
                            <h2 class="border-short-bottom text-center">รายละเอียด</h2>
                    </div>
                            <div class="col-sm-3 mb-3">
                                    <div class="card h-100">
                                        <div class="card-body">
                                                <i class="fa fa-address-card py-2 fa-4x text-info" aria-hidden="true"></i>
                                                <h4 class="card-title">ที่อยู่</h4>
                                                <p class="card-text">กรุงเทพมหานคร ภาษีเจริญ บางหว้า</p>
                                            </div>
                                        </div>
                                    </div>
                            <div class="col-sm-3 mb-3">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <i class="fa fa-phone-square py-2 fa-4x text-info" aria-hidden="true"></i>
                                        <h4 class="card-title">เบอร์โทรศัพท์</h4>
                                        <p class="card-text">กรุงเทพมหานคร ภาษีเจริญ บางหว้า</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 mb-3">
                                    <div class="card h-100">
                                        <div class="card-body">
                                            <i class="fa fa-envelope py-2 fa-4x text-info" aria-hidden="true"></i>
                                            <h4 class="card-title">อีเมล์</h4>
                                            <p class="card-text">กรุงเทพมหานคร ภาษีเจริญ บางหว้า</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 mb-3">
                                        <div class="card h-100">
                                            <div class="card-body">
                                                <i class="fa fa-line py-2 fa-4x text-info" aria-hidden="true"></i>
                                                <h4 class="card-title">ไลน์</h4>
                                                <p class="card-text">กรุงเทพมหานคร ภาษีเจริญ บางหว้า</p>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title"> แบบฟอร์มติดต่อเรา </h5>
                                        <form>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="name">ชื่อ</label>
                                                    <input type="text" id="name" class="form-control" placeholder="ชื่อของคุณ">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="phone">เบอร์โทรศัพท์</label>
                                                    <input type="text" id="phone" class="form-control" placeholder="เบอร์โทรศัพท์ของคุณ">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="email">อีเมลล์</label>
                                                    <input type="text" id="email" class="form-control" placeholder="example@email.com">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="message">ข้อความของคุณ</label>
                                                <textarea id="message" rows="5" class="form-control" placeholder="เขียนข้อความของคุณที่นี้"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary d-block mx-auto">ส่งข้อความ</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>     

                

       
    
            
                       
                <?php include_once('includes/footer.php')?>
            
        
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
        <script src="assets/js/main.js"></script>
    </body>
</php>
