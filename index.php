<?php 
require_once('Connect/connectdb.php');



$sql = "SELECT * FROM `articles` WHERE `status` = '1'";
$result = $conn->query($sql);


if (!$result) {
    header('Location: blog.php');
}
/*
$arr = 0;
$sum = 0;
$x = 0;
$y = 5;
$n = 2;
for($i = 1; $i < 7; $i++){
 
    $t = $x + $i;
     $sum = ($n * $i);
    // $sum2 = ($sum  $i);
   
     //echo $t;

}

for($i=0;$i<=5;$i++){  
    for($j=1;$j<=$i;$j++){  
    echo $j; 
     
    }  
    echo "<br>";  
    }  



*/
?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!--Navbar-->
     <?php include_once('includes/navbar.php')?>

            <section id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="carousel-img" style="background-image: url('https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=74be18f074e19e06a51221f0f09969df&auto=format&fit=crop&w=1280&q=80') ;">
                                <div class="carousel-caption">
                                    <h1 class="display-4 font-weight-bold">AppzStory Studio</h1>
                                    <p class="lead">สังคมของการเขียนเว็บไซต์ (HTML CSS JAVASCRIPT)</p>
                                </div>
                                <div class="backscreen"></div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="carousel-img" style="background-image: url('https://images.unsplash.com/photo-1460925895917-afdab827c52f?ixlib=rb-0.3.5&s=d8791fe0f9f4e735158400f9daf6a558&auto=format&fit=crop&w=1280&q=80') ;">
                                <div class="carousel-caption">
                                    <h1 class="display-4 font-weight-bold">AppzStory Studio</h1>
                                    <p class="lead">สังคมของการเขียนเว็บไซต์ (Bootstrap4 PHP MySQL)</p>
                                </div>
                                <div class="backscreen"></div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="carousel-img" style="background-image: url('https://images.unsplash.com/photo-1488590528505-98d2b5aba04b?ixlib=rb-0.3.5&s=17460aa3d0fd3eb2fb7162edafc37175&auto=format&fit=crop&w=1280&q=80') ;">
                                <div class="carousel-caption">
                                    <h1 class="display-4 font-weight-bold">AppzStory Studio</h1>
                                    <p class="lead">สังคมของการเขียนเว็บไซต์ (AngularJS VueJS Firebase)</p>
                                </div>
                                <div class="backscreen"></div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </section>    


                <section class="jumbotron jumbotron-fluid text-center">
                        <div class="container">
                          <h1 class="border-short-bottom">ความคาดหวังของผู้สอน</h1>
                          <p class="lead">คาดหวังไว้ว่า จะสอนนักเรียนทุกคน ให้สามารถสร้างเว็บไซต์ขึ้นมาด้วยตัวเอง  และเรียนรู้องค์ประกอบ ทุกอย่างที่จำเป็นต่อการเริ่มสร้างเว็บไซต์ขึ้นมา เพื่อให้สามารถประกอบอาชีพ, เข้าสมัครงาน, ทำโปรเจคจบ, หรือทำโปรเจคที่ตัวเองคาดหวังไว้ ให้สำเร็จ</p>
                        </div>
                </section>

    
                        <div class="container">
                          <h1 class="border-short-bottom text-center">Blog</h1>
                        
    
                          <div class="row">
                <?php if($result->num_rows){
                    while($row = $result->fetch_assoc()){
                        ?>
                        <section class="col-12 col-sm-6 col-md-4 p-2">
                            <div class="card">
                                    <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="warpper-card-img">
                                    <img src="<?php echo $row['image'] ?>" class="card-img-top">
                                    </a>
                                    <div class="card-body">
                                      <h5 class="card-title"> <?php echo $row['subject'] ?></h5>
                                      <p class="card-text"><?php echo $row['sub_title'] ?></p>
                                      
                                    </div>
                                    <div class="p-3">
                                            <a href="blog-detail.php?id=<?php echo $row['id'] ?>" class="btn btn-primary btn-block">อ่านเพิ่มเติม</a>
                                    </div>
                    
                            </div>
                            </section> 
                            <?php } }else { ?>
                             <section class="col-12">
                                  <p class="text-center">ไม่มีข้อมูล</p>
                              </section>  
                            <?php 
                            }
                            ?>

                       
                         
                            
                        </div>
                    </div>
                        <?php include_once('includes/footer.php')?>

            
        
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="node_modules/popper.js/dist/umd/popper.js"></script>
        <script src="node_modules/jarallax/dist/jarallax.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOgU18_tVZdK-nJ0iDuutPnbUsTYwE_XA&callback=initMap"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
